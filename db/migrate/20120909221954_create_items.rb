class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.text :overview
      t.string :link

      t.timestamps
    end
  end
end
