class AddSearchIndexToItems < ActiveRecord::Migration
  def up
  	execute "create index items_name on items using gin(to_tsvector('english', name))"
  	execute "create index items_overview on items using gin(to_tsvector('english', overview))"
  end

  def down
  	execute "drop index items_name"
  	execute "drop index items_overview"
  end
end
