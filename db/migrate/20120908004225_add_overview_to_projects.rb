class AddOverviewToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :overview, :text
  end
end
