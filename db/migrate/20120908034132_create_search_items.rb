class CreateSearchItems < ActiveRecord::Migration
  def change
    create_table :search_items do |t|
      t.string :name
      t.text :body
      t.string :link

      t.timestamps
    end
  end
end
