class Simtk.Views.Result extends Backbone.View

	template: JST['items/result']

	render: ->
		$(@el).html(@template(model: @model))
		this