class Simtk.Views.ItemsIndex extends Backbone.View

	initialize: ->
		@typingTimer = 0
		@doneTypingInterval = 250
		@keywords = ""

	template: JST['items/index']

	events:
		'keyup #keywords': 'beginSearchSequence'

	render: ->
		$(@el).html(@template())
		this

	beginSearchSequence: =>
		clearTimeout @typingTimer
		if ($("#keywords").val())
			@typingTimer = setTimeout(@finishSearch, @doneTypingInterval)



	finishSearch: =>
		query = $("#keywords").val()
		if query != @keywords
			@keywords = query
			results = new Simtk.Collections.Items({query: query})
			results.fetch
				success: (collection, response) =>
					$("#resultList").empty()
					results.each (result) =>
						@appendResult(result)


	appendResult: (result) =>
		view = new Simtk.Views.Result(model: result)
		$("#resultList").append(view.render().el)
