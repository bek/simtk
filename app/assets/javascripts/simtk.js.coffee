window.Simtk =
	Models: {}
	Collections: {}
	Views: {}
	Routers: {}
	init: -> 
		new Simtk.Routers.Items()
		Backbone.history.start({pushState: true})

$(document).ready ->
	Simtk.init()
