class Simtk.Collections.Items extends Backbone.Collection

	initialize: (options)->
		@query = options.query

	url: ->
		'/api/items?q='+ @query

	model: Simtk.Models.Item
