class Simtk.Routers.Items extends Backbone.Router

	routes:
		'': 'index'

	index: ->
		view = new Simtk.Views.ItemsIndex()
		$("#container").html(view.render().el)