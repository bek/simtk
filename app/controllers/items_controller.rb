class ItemsController < ApplicationController
  respond_to :json
  
  def index
    @results = Item.search_text(params[:q])
    @response = @results.to_a
    respond_with @response
  end
  

end
