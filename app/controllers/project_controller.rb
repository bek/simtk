class ProjectController < ApplicationController

  respond_to :json

  def index
  	@projects = Project.all
  end

end
