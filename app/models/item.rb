class Item < ActiveRecord::Base
  attr_accessible :link, :name, :overview

  include PgSearch

  pg_search_scope :search_text, 
  				  :against => {:name => 'A', :overview => 'B'}, 
  				  :using => {
  				  	:tsearch => {:prefix => true, 
  				  				 :dictionary => "english", 
  				  				 :normalization => 1 }}


end
