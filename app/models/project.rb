class Project < ActiveRecord::Base
  attr_accessible :body, :name, :link

  after_save :create_item

  def create_item
    Item.find_or_create_by_name_and_overview(:name => name, :overview => overview, :link => link)
  end

  def collect_projects
  	a = Mechanize.new { |agent|
  		agent.user_agent_alias = 'Mac Safari'
	}
	page = a.get("https://simtk.org/")
	begin
		# while true
			results = page.forms.first.submit
			store_results(results)
			while there_is_more(results)
				next_link = get_next_link(results)
				results = next_link.click
				store_results(results)
			end
		# end
	rescue => e
		logger.error "ERROR: #{e.message}"
	end
  end

  def update_overview_attribute
  	Project.all.each do |project|
  		html_doc = Nokogiri::HTML(project.body)
  		element = get_description(html_doc)
  		if element
  			text = Sanitize.clean(element.inner_text).lstrip
  			project.overview = text
  			project.save
      end  		
  	end
    update_for_publications
  end

  def update_for_publications
    Project.where(:overview => nil).each do |p|
      begin
        html_doc = Nokogiri::HTML(p.body)
        text = Sanitize.clean(html_doc.xpath("//table//table//tr[1]//td[1]")[5].inner_text)
        p.overview = text
        p.save
      rescue Exception => e
        logger.error "ERROR: #{e.message}"
      end
    end
    update_for_remaining
  end

  def update_for_remaining
    Project.where(:overview => nil).each do |p|
      begin
        html_doc = Nokogiri::HTML(p.body)
        text = Sanitize.clean(html_doc.xpath("//table//table//tr[1]//td[1]")[3].text)
        p.overview = text
        p.save
      rescue Exception => e
        logger.error "ERROR: #{e.message}"
      end
    end
  end

  def get_description(html)
  	html.search(:td).detect {|td| Sanitize.clean(td.inner_text).lstrip.starts_with?("Description:")}
  end

  def store_results(results)
  	begin
  		results.links_with(:dom_class => "linksHeader3").each do |link|
  			begin
	  			response = link.click
	  			# sleep 0.5
	  			unless Project.find_by_name("#{link.text}")
	  			 	Project.create(:name => link.text, :body => response.body, :link => "https://simtk.org#{link.href}")
	  			end
  			rescue => e
  				logger.error "ERROR: #{e.message}"
  			end
  		end
  	rescue => e
  		logger.error "ERROR: #{e.message}"
  	end
  end

  def there_is_more(results)
  	links = results.links_with(:href => /(offset)/)
  	links.any? {|link| link.text.include?("Next")} 
  end

  def get_next_link(results)
  	links = results.links_with(:href => /(offset)/)
  	links.detect {|link| link.text.include?("Next")}
  end

end
