require "thinking_sphinx/deploy/capistrano"


before 'deploy:update_code', 'thinking_sphinx:stop'
after 'deploy:update_code', 'thinking_sphinx:start'

namespace :sphinx do 
  desc "Install latest release of sphinx"
  task :install, roles: :app do
    run "#{sudo} add-apt-repository ppa:chris-lea/sphinxsearch"
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install sphinxsearch"
  end
  after "deploy:install", "sphinx:install"

  desc "Symlink Sphinx Indexes"
  task :symlink_indexes, roles: :app do
    run "ln -nfs #{shared_path}/db/sphinx #{release_path}/db/sphinx"
  end
  after 'deploy:finalize_update', 'sphinx:symlink_indexes'
  
end